<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Control Vehicular BUAP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- necesario para que Bootstrap vuelva responsiva la pag-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">

                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
            <a class="navbar-brand" href="#">
                <img class="hidden-xs" src="img/SVU.png" alt="">
                <img class="visible-xs" src="img/SVU.png" alt="">    
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <a href="#logIn" data-toggle="modal" class="btn btn-default">Entrar</a>
                <a href="registroAlumno.php" class="btn btn-default">Registrarse</a>
                <a href="registroSISU.php" class="btn btn-default">SISU</a>
                <!--<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Registrarse<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="registroAlumno.php">Alumno</a></li>
                        <li><a href="registroTrabajador.php">Trabajador</a></li>
                    </ul>
                </li>-->
            </ul>
        </div>
    </div>
</nav>
    
    <div class="col-sm-6">
        <a href="#" class="thumbnail home-thumb">
            <img src="img/estacionamiento.jpg" alt="Appi Landing Page Template">
        </a>
        <h3>¿Qué es el Sistema Vehicular Universitario?</h3>
        <p align="justify">En el Sistema Vehicular Universitario contamos con un mapeo de todos los estacionamientos en dentro de Ciudad Universitaria, además de tener el registro de los usuarios.</p>
        <p align="justify">De esta manera podemos llevar un mejor control acerca de la cantidad de vehículos dentro de Ciudad Universitaria, así como su posición.</p>
        <p align="justify">Esto nos ayuda a construir una comunidad universitaria bien diseñada y comunicada. </p>
    </div>

    <div class="col-sm-6">
        <a href="#" class="thumbnail home-thumb">
            <img src="img/interior.jpg" alt="Appi Landing Page Template">
        </a>
        <h3>¿Tienes un auto?</h3>
        <p align="justify">Es importante registrarte como usuario del estacionamiento universitario, y hacerlo es tan sencillo como tramitar tu credencial de estudiante.</p>
        <p align="justify">Al registrarte en el sistema, tienes beneficios tales como:</p>
            <li>Saber donde hay un lugar disponible para estacionarte.</li>
            <li>Hacer una queja o sugerencia acerca del sistema, y de los usuarios mismos.</li>
            <li>Visualizar las multas que pueda tener tu vehículo, así como el reporte bien definido.</li>
            <li>Pagar la multa.</li>
    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <div class="modal fade" id="logIn" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="GET" action="login.php">
                    <div class="modal-header">
                        <h4>Log In<h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="contact-id" class="col-sm-2 control-label">Matrícula: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="n_usuario" id="n_usuario" placeholder="201222077" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-password" class="col-sm-2 control-label">Contraseña: </label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="pwd" id="pwd" placeholder="password" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cerrar</a>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <!-- importando JQuery-->
    <script src="js/bootstrap.js"></script> <!-- importando JQuery-->
</body>
</html>