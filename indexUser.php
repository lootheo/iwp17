<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Control Vehicular BUAP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- necesario para que Bootstrap vuelva responsiva la pag-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<?PHP session_start(); 
    if(!isset($_SESSION['user']) || $_SESSION['tipo'] == "admin")
        header("Location: index.php"); ?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">

                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
            <a class="navbar-brand" href="salir.php">
                <img class="hidden-xs" src="img/SVU.png" alt="">
                <img class="visible-xs" src="img/SVU.png" alt="" href="salir.php">    
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Reportes</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quejas y Sugerencias <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#comment" data-toggle="modal">Hacer un comentario</a></li>
                        <li><a href="#">Ver todos los comentarios</a></li>
                    </ul>
                <li><a href="salir.php">Cerrar Sesión</a></li>
                <a href="#">
                    <img src="img/fotoUser.png" alt="">    
                </a>
            </ul>
        </div>
    </div>
</nav>

    <div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15089.701503140554!2d-98.20933768998238!3d19.000968582741812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfbf5e12520371%3A0x471e3afdf138fca5!2sCd+Universitaria%2C+72592+Puebla%2C+Pue.!5e0!3m2!1sen!2smx!4v1458973183870" width="1350" height="470" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <div class="modal fade" id="comment" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="insertComment.php">
                    <div class="modal-header">
                        <h4>Queja o Sugerencia<h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="contact-name" class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <input type="radio" name="opt" value="estacionamiento" >Sobre la comunidad
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-name" class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <input type="radio" name="opt" value="aplicacion" >Sobre el sistema
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-message" class="col-sm-2 control-label">Comentario</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="4" name="comm"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <!-- importando JQuery-->
    <script src="js/bootstrap.js"></script> <!-- importando JQuery-->
</body>
</html>