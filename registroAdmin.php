<!DOCTYPE html>
<html lang="es">
<head>
	<title>Control Vehicular BUAP</title>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- necesario para que Bootstrap vuelva responsiva la pag-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/registro.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <script type="text/javascript">

      document.addEventListener("DOMContentLoaded", function() {

        // JavaScript form validation

        var checkPassword = function(str)
        {
          var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
          return re.test(str);
        };
        var checkNombre = function(str)
        {
          var redb = /^(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
          return re.test(str);
        };
        var checkTel = function(str)
        {
          var reTM = /[0-9]/;
          return re.test(str);
        };

        var checkForm = function(e)
        {
          if(this.nombre.value == "") {
            alert("Error: ¡Nombre no puede estar en blanco!");
            this.nombre.focus();
            e.preventDefault(); // equivalent to return false
            return;
          }
          /*var reTM = /^[0-9]$/;
          if(isNaN(this.tel.value)==true || /^[0-9]\d$/.test(this.tel.value)==false ){
            alert("Error: ¡Sólo deben ser números!");
            this.tel.focus();
            e.preventDefault();
          } 
          if(/^[0-9]$/.test(this.matricula.value)==false){
            alert("Error: ¡Sólo deben ser números!");
            this.matricula.focus();
            e.preventDefault();
          }*/
          if(this.pass.value != "" && this.pass.value == this.pass1.value) {
            if(!checkPassword(this.pass.value)) {
              alert("¡Contraseña no válida!");
              this.pass.focus();
              e.preventDefault();
              return;
            }
          } else {
            alert("Error: ¡Verifica que confirmaste tu contraseña!");
            this.pass.focus();
            e.preventDefault();
            return;
          }
          alert("¡Registro Exitoso!");
        };

        var myForm = document.getElementById("myForm");
        myForm.addEventListener("submit", checkForm, true);

        // HTML5 form validation

        var supports_input_validity = function()
        {
          var i = document.createElement("input");
          return "setCustomValidity" in i;
        }

        if(supports_input_validity()) {
          var usernameInput = document.getElementById("field_username");
          usernameInput.setCustomValidity(usernameInput.title);

          /*var telInput = document.getElementById("field_tel");
          telInput.setCustomValidity(telInput.title);
          var matInput = document.getElementById("field_mat");
          matInput.setCustomValidity(matInput.title);*/

          var pwd1Input = document.getElementById("field_pwd1");
          pwd1Input.setCustomValidity(pwd1Input.title);

          var pwd2Input = document.getElementById("field_pwd2");

          // input key handlers

          usernameInput.addEventListener("keyup", function() {
            usernameInput.setCustomValidity(this.validity.patternMismatch ? usernameInput.title : "");
          }, false);

          pwd1Input.addEventListener("keyup", function() {
            this.setCustomValidity(this.validity.patternMismatch ? pwd1Input.title : "");
            if(this.checkValidity()) {
              pwd2Input.pattern = this.value;
              pwd2Input.setCustomValidity(pwd2Input.title);
            } else {
              pwd2Input.pattern = this.pattern;
              pwd2Input.setCustomValidity("");
            }
          }, false);

          pwd2Input.addEventListener("keyup", function() {
            this.setCustomValidity(this.validity.patternMismatch ? pwd2Input.title : "");
          }, false);

        }

      }, false);

    </script>
</head>
<body>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">

                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
            <a class="navbar-brand" href="#">
                <img class="hidden-xs" src="img/SVU.png" alt="">
                <img class="visible-xs" src="img/SVU.png" alt="">    
            </a>
        </div>
    </div>
</nav>


    <div class="registro">
    <div>
		<h2 id="tableTitle" align="center">Registro Administrador</h2>
	</div>
        
        <form id="myForm" method="POST" action="registerAdmin.php">
        	<h2 class="datos">Datos del Administrador</h2>
        	
            <div class="datosPersonales">
        		<div class="row" >
        			<div class="col-xs-4" align="right">
        				Nombre
        			</div>
        			<div class="col-xs-8">
        				<input type="text" class="text" id="field_username" name="nombre" title="¡Nombre requerido!" required></input>
        			</div>
        		</div>
                <br>
                <br>
                <div class="row">
                    <div class="col-xs-4" align="right">
                        ID de trabajador
                    </div>
                    <div class="col-xs-8">
                        <input type="text" class="text" id="field_mat" name="matricula" title="¡Introducir matrícula!" required></input>
                    </div>
                </div>
        		<br>
        		<br>
        		<div class="row">
        			<div class="col-xs-4" align="right">
        				E-mail
        			</div>
        			<div class="col-xs-8">
        				<input type="email" class="text" name="email" required></input>
        			</div>
        		</div>
                <br>
                <br>
                <div class="row">
                    <div class="col-xs-4" align="right">
                        Teléfono
                    </div>
                    <div class="col-xs-8">
                        <input type="text" class="text" id="field_tel" name="tel" title="¡Introducir número celular!" required></input>
                    </div>
                </div>
        		<br>
        		<br>
        		<div class="row">
        			<div class="col-xs-4" align="right">
        				Contraseña
        			</div>
        			<div class="col-xs-8">
        				<input type="password" class="text" id="field_pwd1" name="pass" title="La contraseña debe tener mínimo 6 caracteres, incluyendo mayúsculas, minúsculas y números." required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"></input>
        			</div>
        		</div>
                <br>
                <br>
                <div class="row">
                    <div class="col-xs-4" align="right">
                        Confirmar
                    </div>
                    <div class="col-xs-8">
                        <input type="password" class="text" id="field_pwd2" name="pass1" title="Contraseñas diferentes." required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"></input>
                    </div>
                </div>
            </div>

        		
            <div align="center">
            	<button type="submit" class="btnRegistrarse">Registrarse</button>
            </div>
            

        </form>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <div class="modal fade" id="logIn" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="GET" action="changeToAdmin.php">
                    <div class="modal-header">
                        <h4>Código de Acceso para administrador<h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="contact-password" class="col-sm-2 control-label">Código: </label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="pwd" id="pwd" placeholder="password" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cerrar</a>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <!-- importando JQuery-->
	<script src="js/bootstrap.js"></script> <!-- importando JQuery-->
</body>
</html>